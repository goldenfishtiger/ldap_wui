# coding: utf-8

class CompatCGI

	attr_reader :params

	def initialize(request)

		query = ''
		if(request.env['REQUEST_METHOD'] =~ /GET/i)
			query = request.query_string

		elsif(request.env['REQUEST_METHOD'] =~ /POST/i)
			request.body.rewind
			query = request.body.read
		end
		@params = Rack::Utils.parse_query(query)

		def @params.[](key)
			value = super(key)
			value.is_a?(Array) and return(value)
			value.is_a?(String) and return([value])
			value.is_a?(NilClass) and return([])
			raise('Unexpected error.')
		end
	end
end

__END__

